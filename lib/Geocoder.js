const googleMaps = require('@google/maps');

class Geocoder {
	constructor() {
		this.api = googleMaps.createClient({
			key: process.env.GOOGLE_APIKEY,
			Promise: global.Promise
		});
	}

	getGeocodeResults(xhr) {
		if (xhr.status === 'ZERO_RESULTS')
			return null;
		if (xhr.status !== 'OK')
			throw new Error('Internal error contacting Google.');

		let result = xhr.results[0];
		return {
			formatted_address: result.formatted_address,
			lat: result.geometry.location.lat,
			lon: result.geometry.location.lng
		};
	}

	async locate(address) {
		let xhr = await this.api.geocode({address}).asPromise();
		return this.getGeocodeResults(xhr.json);
	}
}

module.exports = Geocoder;
