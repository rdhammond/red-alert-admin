const _ = require('underscore');
const Expo = require('expo-server-sdk').Expo;
const PushToken = require('../models/PushToken');

class Pusher {
	constructor() {
		this.expo = new Expo();
	}

	createPushMessage(expoToken, srcMessage) {
		let message = _.pick(srcMessage,
			'title', 'body', 'data'
		);

		return {
			to: expoToken,
			sound: 'default',
			title: srcMessage.title,
			body: srcMessage.body,
			data: srcMessage.data || {}
		};
	}

	createMessageChunks(tokens, srcMessage) {
		let expoTokens = _.map(tokens, x => x.expoToken);
		let messages = [];

		for (let expoToken of expoTokens) {
			if (!Expo.isExpoPushToken(expoToken))
				continue;

			let message = this.createPushMessage(expoToken, srcMessage);
			messages.push(message);
		}
		return this.expo.chunkPushNotifications(messages);
	}

	async pushMessageChunks(messageChunks) {
		let tickets = [];
		for (let messageChunk of messageChunks) {
			let ticketChunk = await this.expo.sendPushNotificationsAsync(messageChunk);
			tickets.push(...ticketChunk);
		}
		return tickets;
	}

	createReceiptIdChunks(tickets) {
		let receiptIds = _.map(tickets, x => x.id);
		return this.expo.chunkPushNotificationReceiptIds(receiptIds);
	}
	
	isBadTokenError(receipt) {
		return receipt.status === 'error'
			&& receipt.details
			&& receipt.details.error === 'DeviceNotRegistered'
	}

	getErroredTokens(receipts) {
		let erroredTokens = [];

		for (let receipt of receipts) {
			if (!this.isBadTokenError(receipt))
				continue;

			let erroredToken = this.extractTokenFromError(receipt.message);
			erroredTokens.push(erroredToken);
		}
		return erroredTokens;
	}

	extractTokenFromError(message) {
		return /ExponentPushToken\[[^\]]+\]/.exec(message)[0];
	}

	async send(tokens, message) {
		let messageChunks = this.createMessageChunks(tokens, message);
		return await this.pushMessageChunks(messageChunks);
	}

	async getReceiptChunk(receiptIdChunk) {
		let receipts = await this.expo.getPushNotificationReceiptsAsync(receiptIdChunk);
		if (!_.isArray(receipts))
			receipts = [receipts];
	}

	async cleanupDatabaseEventually(tickets) {
		let receiptIdChunks = this.createReceiptIdChunks(tickets);

		for (let receiptIdChunk of receiptIdChunks) {
			let receiptChunk = this.getReceiptChunk(receiptIdChunk);
			let erroredTokens = this.getErroredTokens(receiptChunk);
			await PushToken.remove({expoToken: {$in: [erroredTokens]}});
		}
	}

	async pushMessage(message) {
		let tokens = await PushToken.find({});
		let tickets = await this.send(tokens, message);
		this.cleanupDatabaseEventually(tickets);
	}
}

module.exports = Pusher;
