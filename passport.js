const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const LocalApiKeyStrategy = require('passport-localapikey').Strategy;
const User = require('./models/User');
const ApiKey = require('./models/ApiKey');

async function verifyLocal(req, email, password, done) {
	try {
		let user = await User.findOne({email});
		return user && user.checkPassword(password)
			? done(null, user)
			: done(null, false);
	}
	catch (err) {
		done(err);
	}
}

async function verifyApiKey(apikeyStr, done) {
	try {
		let apikey = await ApiKey.findOne({apikey: apikeyStr});
		return apikey ? done(null, apikey) : done(null, false);
	}
	catch (err) {
		done(err);
	}
}

function serializeUser(user, done) {
	done(null, user._id);
}

async function deserializeUser(id, done) {
	try {
		let user = await User.findById(id);
		done(null, user);
	}
	catch (err) {
		done(err);
	}
}

let localStrategyOpts = {
	usernameField: 'email',
	passReqToCallback: true
};
let localStrategy = new LocalStrategy(localStrategyOpts, verifyLocal);
passport.use('local', localStrategy);

let localApiKeyStrategy = new LocalApiKeyStrategy(verifyApiKey);
passport.use('localapikey', localApiKeyStrategy);

passport.serializeUser(serializeUser);
passport.deserializeUser(deserializeUser);
module.exports = passport;
