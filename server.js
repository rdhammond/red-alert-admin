const config = require('./config');
const _ = require('underscore');
const mongoose = require('mongoose');

const RouteNames = [
	'Announcement', 'ContactInfo', 'Event', 'Home', 'SignIn', 'SignOut',
	'Tools'
];

const ApiNames = [
	'Announcements', 'ContactInfo', 'Events', 'Locations', 'Home', 'PushTokens'
];

function getRouteClass(routeName) {
	return require(`./routes/${routeName}Route`);
}

function getApiClass(apiName) {
	return require(`./routes/api/${apiName}Api`);
}

function main() {
	mongoose.connect(encodeURI(process.env.MONGODB_URI));
	mongoose.Promise = global.Promise;

	const app = require('./app');

	for (let routeName of RouteNames) {
		let RouteClass = getRouteClass(routeName);
		new RouteClass().register(app);
	}
	for (let apiName of ApiNames) {
		let ApiClass = getApiClass(apiName);
		new ApiClass().register(app);
	}

	let port = process.env.PORT || config.port;
	let startupMessage = `Admin for ${config.name} listening on ${port}`;
	app.listen(port, () => console.log(startupMessage));
}
main();
