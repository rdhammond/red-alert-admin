const mongoose = require('mongoose');

let Announcement = new mongoose.Schema({
	title: {type: String, maxlength: 100, required: true},
	body: {type: String, maxlength: 1000, required: true},
	push: {type: Boolean, default: false},
	sentBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
	sentOn: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Announcement', Announcement);
