const mongoose = require('mongoose');

let schema = new mongoose.Schema({
	apikey: {type: String, required: true},
	ipAddresses: [String]
});

module.exports = mongoose.model('ApiKey', schema);
