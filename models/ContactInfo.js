const mongoose = require('mongoose');

let ContactInfo = mongoose.Schema({
	name: {type: String, required: true},
	email: {type: String, match: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, required: true},
	address1: String,
	address2: String,
	city: String,
	state: {type: String, match: /^[A-Z]{2}$/},
	zipcode: {type: String, match: /^[0-9]{5}$/},
	phone: {type: String, match: /^[0-9]{10}$/}
});

module.exports = mongoose.model('ContactInfo', ContactInfo);
