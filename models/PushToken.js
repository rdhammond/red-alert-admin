const mongoose = require('mongoose');

let schema = new mongoose.Schema({
	expoToken: {type: String, required: true, match: /^ExponentPushToken\[.*\]$/},
	createdOn: {type: Date, default: Date.now}
});

module.exports = mongoose.model('PushToken', schema);
