const mongoose = require('mongoose');
const sodium = require('sodium').api;

let User = new mongoose.Schema({
	email: {
		type: String,
		match: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
		required: true,
		unique: true
	},
	passwordHash: {type: Buffer, required: true},
	createdOn: {type: Date, default: Date.now}
});

User.methods.toJSON = function() {
	let obj = this.toObject();
	delete obj.passwordHash;
	return obj;
};

User.methods.checkPassword = function(pw) {
	return sodium.crypto_pwhash_str_verify(this.passwordHash, Buffer.from(pw, 'utf8'));
};

User.virtual('password').set(function(pw) {
	this.passwordHash = sodium.crypto_pwhash_str(
		Buffer.from(pw, 'utf8'),
		sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE,
		sodium.crypto_pwhash_MEMLIMIT_INTERACTIVE
	);
});

module.exports = mongoose.model('User', User);
