const mongoose = require('mongoose');

let Loc = mongoose.Schema({
	type: String,
	coordinates: [Number]
});

let Event = mongoose.Schema({
	title: {type: String, required: true, minlength: 1, maxlength: 100},
	description: {type: String, required: true, minlength: 1, maxlength: 1000},
	address: {type: String, required: true},
	loc: {type: Loc, required: true},
	eventTime: {type: Date, required: true},
	push: {type: Boolean, default: false},
	createdOn: {type: Date, default: Date.now},
	createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
	updatedOn: {type: Date, default: Date.now},
	updatedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}
});
Event.index({loc: "2dsphere"});

module.exports = mongoose.model('Event', Event);
