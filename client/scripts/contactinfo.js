(function($) {
	'use strict';

	function saveContactInfo() {
		var $contactinfo = $('#contactinfo');
		var data = $contactinfo.serializeJSON();
		data.phone = data.phone.replace(/[^0-9]/g, '');

		$('#contactinfo').formAjax({
			data: JSON.stringify(data),
			success: function(data) {
				$('#alerts').addAlert({
					title: 'Success!',
					body: 'Your changes were saved.',
					type: 'success'
				});
			}
		});
		return false;
	}

	$(function() {
		$('#contactinfo').submit(saveContactInfo);
	});
})(jQuery);
