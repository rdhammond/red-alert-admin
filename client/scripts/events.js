(function($) {
	'use strict';

	function eventTimeFormatter(value) {
		var datetime = new Date(value);
		var date = datetime.toLocaleDateString('en-us');
		var time = datetime.toLocaleTimeString('en-us', {hour12: true, hour: 'numeric', minute: '2-digit'});

		return date + ' @ ' + time;
	}

	function deleteEvent() {
		var $confirmDelete = $('#confirmDelete');
		$confirmDelete.modal('hide');

		$.ajax({
			method: 'DELETE',
			url: '/api/events/' + $confirmDelete.data('id'),
			success: function(data) {
				$('#events').find('.event')
					.filter(function() { return $(this).data('id') === data._id })
					.remove();
			},
			error: function() {
				$('#alerts').addGenericErrAlert();
			}
		});
	}

	function customValidate() {
		var $date = $('#date');
		var $time = $('#time');
		var eventTime = new Date($date.val() + ' ' + $time.val());
		if (isNaN(eventTime)) {
			$('#date')[0].setCustomValidity('Date/time not valid.');
			$('#time')[0].setCustomValidity('Date/time not valid.');
		}
		else {
			$('#date')[0].setCustomValidity('');
			$('#time')[0].setCustomValidity('');
		}
	}

	function sendEvent(e) {
		e.preventDefault();

		var $event = $('#event');
		var ev = $event.serializeJSON();
		ev.eventTime = new Date($('#date').val() + ' ' + $('#time').val());
		delete ev.date;
		delete ev.time;
		ev.loc = {
			type: 'Point',
			coordinates: [ev.lon, ev.lat]
		};
		delete ev.lat;
		delete ev.lon;

		$event.formAjax({
			data: JSON.stringify(ev),
			success: function(data) {
				var $newEvent = $('<div></div>').loadTemplate($('#eventTemplate'), data).children().first();
				$newEvent.data('id', data._id);
				$('#events').prepend($newEvent);
				$('#event')[0].reset();
				$('#title').focus();
			}
		});
		return false;
	}

	function showConfirmDelete() {
		var $confirmDelete = $('#confirmDelete');
		$confirmDelete.data('id', $(this).closest('.event').data('id'));
		$confirmDelete.modal('show');
	}

	function locateAddress() {
		var $address = $('#address');

		function addressNotFound() {
			$('#lat').val('');
			$('#lon').val('');
			$address[0].setCustomValidity('Address not found.');
		}

		$.ajax({
			method: 'POST',
			url: '/api/locations',
			data: JSON.stringify({address: $address.val()}),
			dataType: 'json',
			contentType: 'application/json',
			success: function(data) {
				if (data.loc) {
					$('#address').val(data.loc.formatted_address);
					$('#lat').val(data.loc.lat);
					$('#lon').val(data.loc.lon);
					$address[0].setCustomValidity('');
				}
				else
					addressNotFound();
					
				$('#event button[type="submit"]').prop('disabled', false);
			},
			error: function() {
				addressNotFound();
				$('#event button[type="submit"]').prop('disabled', false);
			}
		});
		$('#event button[type="submit"]').prop('disabled', true);
	}

	$(function() {
		$.addTemplateFormatter({EventTimeFormatter: eventTimeFormatter});

		// We don't want to cache these.
		$('#address').val('');
		$('#lat').val('');
		$('#lon').val('');

		$('#address').change(locateAddress);
		$('#event button[type="submit"]').click(customValidate);
		$('#event').submit(sendEvent);
		$('#events').on('click', '.event .close', showConfirmDelete);
		$('#confirmDelete button.confirm').click(deleteEvent);
	});

})(jQuery);
