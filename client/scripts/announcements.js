(function($) {
	'use strict';

	function addAnnouncement(data) {
		var $announcement = $('<div></div>')
			.loadTemplate($('#announcementTemplate'), data)
			.children()
			.first();

		console.log(data);
		$announcement.data('id', data._id);
		$('#announcements').prepend($announcement);
	}

	function deleteAnnouncement() {
		var $confirmDelete = $('#confirmDelete');
		$confirmDelete.modal('hide');

		$.ajax({
			method: 'DELETE',
			url: '/api/announcements/' + $confirmDelete.data('id'),
			success: function(data) {
				$('#announcements').find('.announcement')
					.filter(function() { return $(this).data('id') === data._id; })
					.remove();
			},
			error: function() {
				$('#announcements').addGenericErrorAlert();
			}
		});
	}

	function showConfirmDelete() {
		var $confirmDelete = $('#confirmDelete');
		$confirmDelete.data('id', $(this).closest('.announcement').data('id'));
		$confirmDelete.modal('show');
	}

	function sendAnnouncement(e) {
		e.preventDefault();

		var $announcement = $('#announcement');
		$announcement.formAjax({
			success: function(data) {
				var sentOn = new Date(data.sentOn);
				var date = sentOn.toLocaleDateString('en-us');
				var time = sentOn.toLocaleTimeString('en-us', {hour12: true, hour: 'numeric', minute: '2-digit'});
				data.footer = date + ' @ ' + time;
				addAnnouncement(data);

				$('#alerts').addAlert({
					title: 'Success!',
					body: 'Your announcement should arrive shortly.',
					type: 'success'
				});
				$announcement[0].reset();
				$('#title').focus();
			}
		});
		return false;
	}

	$(function() {
		$('#announcement').submit(sendAnnouncement);
		$('#announcements').on('click', '.announcement .close', showConfirmDelete);
		$('#confirmDelete button.confirm').click(deleteAnnouncement);
	});
})(jQuery);
