(function($) {
	'use strict';

	function confirm() {
		$.ajax({
			method: 'DELETE',
			url: '/api',
			error: function() {
				$('#alerts').addGenericErrAlert();
			},
			success: function() {
				$('#alerts').addAlert({
					title: 'Success!',
					body: 'Site has been reset.',
					type: 'success'
				});
			}
		});
		$('#confirmReset').modal('hide');
	}

	$(function() {
		$('#confirmReset .confirm').click(confirm);
	});
}(jQuery));
