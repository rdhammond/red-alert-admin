(function($) {
	'use strict';

	$.fn.addGenericErrAlert = function() {
		this.addAlert({
			title: 'Failed to send.',
			body: 'Please try again in a little bit.',
			type: 'error'
		});
	};

	$.fn.addAlert = function(opts) {
		var data = $.extend(opts, {type: 'primary'});

		var $alert = $('<div></div>');
		$alert.loadTemplate($('#alertTemplate'), data);
		$alert.addClass('in alert-' + data.type);
		$alert.appendTo(this);
		$alert.alert();
	};

	$.fn.formAjax = function(opts) {
		var data = $.extend({}, {
			method: this.attr('method'),
			url: this.attr('action'),
			dataType: 'json',
			data: JSON.stringify(this.serializeJSON()),
			contentType: 'application/json',
			error: function() {
				$('#alerts').addGenericErrAlert();
			}
		}, opts);

		console.log(data.data);
		$.ajax(data);
		return this;
	};

})(jQuery);
