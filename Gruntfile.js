module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Copy web assets from bower_components to more convenient directories.
		copy: {
			main: {
				files: [
					// Vendor scripts.
					{
						expand: true,
						cwd: 'bower_components/bootstrap/dist/js/',
						src: ['**/*.js'],
						dest: 'public/scripts/bootstrap/'
					},
					{
						expand: true,
						cwd: 'bower_components/jquery/dist/',
						src: ['**/*.js', '**/*.map'],
						dest: 'public/scripts/jquery/'
					},
					{
						expand: true,
						cwd: 'bower_components/popper.js/dist/',
						src: ['**/*.js', '**/*.map'],
						dest: 'public/scripts/popper/'
					},
					{
						expand: true,
						cwd: 'bower_components/jquery-serializeJSON/lib/',
						src: ['**/*.js', '**/*.map'],
						dest: 'public/scripts/jquery-serializeJSON/'
					},
					{
						expand: true,
						cwd: 'bower_components/jquery-load-template/dist/',
						src: ['**/*.js', '**/*.map'],
						dest: 'public/scripts/jquery-load-template'
					},

					// Icons
					{
						expand: true,
						cwd: 'bower_components/font-awesome/web-fonts-with-css/webfonts/',
						src: ['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff', '**/*.woff2'],
						dest: 'public/fonts/'
					},

					// Fonts
					{
						expand: true,
						flatten: true,
						src: ['bower_components/font-awesome/fonts/*'],
						dest: 'fonts'
					},

					// Images
					{
						expand: true,
						flatten: true,
						src: ['client/images/*'],
						dest: 'public/images/'
					}
				]
			},
		},

		// Compile SASS files into minified CSS.
		sass: {
			options: {
				includePaths: [
					'bower_components/bootstrap/scss',
					'bower_components/font-awesome/web-fonts-with-css/scss/'
				]
			},
			dist: {
				options: {
					outputStyle: 'compressed'
				},
				files: {
					'public/css/app.css': 'client/scss/app.scss'
				}
			}
		},

		// Minify custom JS
		uglify: {
			dist: {
				files: [
					{
						expand: true,
						cwd: 'client/scripts',
						src: '**/*.js',
						dest: 'public/scripts',
						rename: function(dst, src) {
							return dst + '/' + src.replace('.js', '.min.js');
						}
					},
					{
						expand: true,
						cwd: 'bower_components/jquery-serializeJSON/lib',
						src: '**/*.js',
						dest: 'public/scripts/jquery-serializeJSON',
						rename: function(dst, src) {
							return dst + '/' + src.replace('.js', '.min.js');
						}
					}
				]
			}
		}
	});

	// Load externally defined tasks. 
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Establish tasks we can run from the terminal.
	grunt.registerTask('build', ['sass', 'copy', 'uglify']);
	grunt.registerTask('default', ['build']);
}
