const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const config = require('./config');
const passport = require('./passport');

const sessionOpts = {
	secret: process.env.SESSION_SECRET,
	resave: false,
	saveUninitialized: false
};

function setBrand(req, res, next) {
	res.locals.brand = config.brand;
	next();
}

let app = express();
app.set('views', 'views');
app.set('view engine', 'pug');
app.use(express.static('public'));
app.use(session(sessionOpts));
app.use(setBrand);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

module.exports = app;
