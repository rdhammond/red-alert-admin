const readline = require('readline');
const crypto = require('crypto');

const mongoose = require('mongoose');
console.log(process.env.MONGODB_URI);
mongoose.connect(process.env.MONGODB_URI);

const ApiKey = require('../models/ApiKey');

function SeedKey(line) {
	return crypto.createHash('md5').update(line).digest('hex');
}

function ParseIps(line) {
	return line.split(',');
}

async function Finish(apikeyStr, ipAddresses) {
	let apikey = new ApiKey({apikey: apikeyStr, ipAddresses});
	await apikey.save();
}

function main(done) {
	console.log('This tool will initialize public API keys for the site.');

	let state = 0;
	let con = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
		prompt: 'Random string for md5 generation: '
	});

	var apikey, ipAddresses;

	con.prompt();
	con.on('line', async line => {
		switch (state) {
			case 0:
				apikey = SeedKey(line);
				if (apikey) {
					con.setPrompt('IP Addresses (comma delimited, optional): ');
					state = 1;
				}
				break;
			case 1:
				ipAddresses = ParseIps(line);

				try {
					await Finish(apikey, ipAddresses);
					console.log('Success! API key should be ready to go.');
					con.close();
					return done();
				}
				catch (err) {
					console.log(`ERROR: ${err}`);
					con.close();
					process.exit(1);
				}
				break;
		}
		con.prompt();
	});
}

main(() => process.exit(0))
