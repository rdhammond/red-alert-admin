const readline = require('readline');

const config = require('../config');
const mongoose = require('mongoose');
mongoose.connect(config.mongodb.connStr);

const User = require('../models/User');

function ProcessUsername(line) {
	if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(line)) {
		console.log('Not a valid email address.');
		return null;
	}
	return line;
}

function ProcessPassword(line) {
	if (line.length < 8 || /^[a-z]{1,12}$/.test(line)) {
		console.log('Password is too weak. Try something longer than eight characters that has more than just lowercase letters in it.');
		return null;
	}
	return line;
}

function Finish(email, password) {
	let user = new User({email});
	user.password = password;
	return user.save();
}

function main() {
	console.log('This tool will initialize the admin user for the site. PLEASE use a strong, RANDOM password!');

	let state = 0;
	let con = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
		prompt: 'Admin email: '
	});

	var email, password;

	con.prompt();
	con.on('line', line => {
		switch (state) {
			case 0:
				email = ProcessUsername(line);
				if (email) {
					con.setPrompt('Password: ');
					state = 1;
				}
				break;
			case 1:
				password = ProcessPassword(line);
				if (!password) break;

				Finish(email, password).then(() => {
					console.log('Success! Admin login should be ready to go.');
					con.close();
					process.exit();
				})
				.catch(err => {
					console.log(`ERROR: ${err}`);
					con.close();
					process.exit(1);
				});
				return;
		}
		con.prompt();
	});
}

User.find({}).count().then(cnt => {
	if (cnt > 0) {
		console.log('This tool is only meant be used for new setups.');
		process.exit(1);
		return;
	}
	
	main();
})
.catch(err => {
	console.error(err);
	process.exit(1);
});
