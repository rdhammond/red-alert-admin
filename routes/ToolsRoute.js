const Route = require('./Route');

class ToolsRoute extends Route {
	register(app) {
		app.route('/tools')
			.all(this.pre.bind(this))
			.get(this.authHtml, this.get.bind(this));
	}

	pre(req, res, next) {
		res.locals.current = 'tools';
		next();
	}

	get(req, res) {
		res.render('tools');
	}
}

module.exports = ToolsRoute;
