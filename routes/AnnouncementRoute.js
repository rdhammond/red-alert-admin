const _ = require('underscore');
const Route = require('./Route');
const Announcement = require('../models/Announcement');

class AnnouncementRoute extends Route {
	register(app) {
		app.route('/announcements')
			.all(this.pre.bind(this))
			.get(this.authHtml, this.get.bind(this));
	}

	pre(req, res, next) {
		res.locals.current = 'announcements';
		next();
	}

	async get(req, res, next) {
		try {
			let announcements = await Announcement
				.find({})
				.sort({sentOn: -1});

			res.locals.announcements = announcements;
			res.render('announcements');
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = AnnouncementRoute;
