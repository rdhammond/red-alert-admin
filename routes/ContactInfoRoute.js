const Route = require('./Route');
const ContactInfo = require('../models/ContactInfo');

class ContactInfoRoute extends Route {
	register(app) {
		app.route('/contactinfo')
			.all(this.pre.bind(this))
			.get(this.authHtml, this.get.bind(this));
	}

	pre(req, res, next) {
		res.locals.current = 'contactinfo';
		next();
	}

	async get(req, res, next) {
		try {
			let info = await ContactInfo.findOne({});
			res.locals.info = info || {};
			res.render('contactinfo');
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = ContactInfoRoute;
