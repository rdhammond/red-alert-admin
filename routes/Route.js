const passport = require('passport');
const ApiKey = require('../models/ApiKey');

class Route {
	authHtml(req, res, next) {
		res.locals.authenticated = req.isAuthenticated();
		if (res.locals.authenticated)
			return next();
		res.redirect('/signin');
	}

	authJson(req, res, next) {
		res.locals.authenticated = req.isAuthenticated();
		if (res.locals.authenticated)
			return next();
		res.sendStatus(401);
	}

	async authApiKey(req, res, next) {
		passport.authenticate('localapikey', {session: false}, async (err, apikey) => {
			console.log(`Got api key from address ${req.connection.remoteAddress}`);
			if (err)
				return next(err);
			if (!apikey)
				return res.sendStatus(401);
			if ((apikey.ipAddresses || []).length < 1)
				return next();

			for (let ipAddress of apikey.ipAddresses) {
				if (req.connection.remoteAddress.indexOf(ipAddress) >= 0)
					return next();
			}
			return res.sendStatus(401);
		})(req, res, next);
	}

	register(app) {
		throw new Error('register() must be implemented.');
	}
}

module.exports = Route;
