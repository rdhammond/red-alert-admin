const Route = require('./Route');

class HomeRoute extends Route {
	register(server) {
		server.get('/', this.get.bind(this));
	}

	get(req, res) {
		if (req.isAuthenticated())
			return res.redirect('/events');
		
		res.redirect('/signin');
	}
}

module.exports = HomeRoute;
