const passport = require('passport'); 
const Route = require('./Route');

const failedLoginAlert = {
	title: 'Signing in failed.',
	body: 'Please check your email and password, then try again.'
};

class SignInRoute extends Route {
	register(app) {
		app.route('/signin')
			.get(this.get.bind(this))
			.post(this.post);
	}
	
	post(req, res, next) {
		passport.authenticate('local', (err, user, info) => {
			try {
				if (err)
					return next(err);

				if (!user) {
					return res.render('signin', {errors: [failedLoginAlert]});
				}

				req.login(user, () => res.redirect('/events'));
			}
			catch (err) {
				return next(err);
			}
		})(req, res, next);
	}

	get(req, res) {
		res.render('signin');
	}
}

module.exports = SignInRoute;
