const Route = require('../Route');
const Announcement = require('../../models/Announcement');
const Event = require('../../models/Event');
const ContactInfo = require('../../models/ContactInfo');
const PushToken = require('../../models/PushToken');

class HomeApi extends Route {
	register(app) {
		app.delete('/api', this.authJson, this.deleteAll.bind(this));
	}

	async deleteAll(req, res, next) {
		try {
			await Promise.all([
				Announcement.remove({}),
				Event.remove({}),
				ContactInfo.remove({}),
				PushToken.remove({})
			]);
			res.sendStatus(200);
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = HomeApi;
