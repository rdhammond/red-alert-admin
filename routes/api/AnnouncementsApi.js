const _ = require('underscore');
const Route = require('../Route');
const Pusher = require('../../lib/Pusher');
const Announcement = require('../../models/Announcement');

class AnnouncementsApi extends Route {
	constructor() {
		super();
		this.pusher = new Pusher();
	}

	register(app) {
		app.route('/api/announcements')
			.get(this.authApiKey, this.all.bind(this))
			.post(this.authJson, this.post.bind(this));

		app.get('/api/announcements/:id', this.authApiKey, this.one.bind(this));
		app.delete('/api/announcements/:id', this.authJson, this.del.bind(this));
	}

	async all(req, res, next) {
		try {
			let announcements = await Announcement
				.find({})
				.sort({sentOn: -1});

			res.json({announcements});
		}
		catch (err) {
			next(err);
		}
	}

	async one(req, res, next) {
		try {
			let announcement = await Announcement.findById(req.params.id);
			res.json(announcement);
		}
		catch (err) {
			next(err);
		}
	}

	async pushAnnouncement(announcement) {
		announcement.data = {type: 'announcement'};
		await this.pusher.pushMessage(announcement);
	}

	async post(req, res, next) {
		try {
			req.body.sentBy = req.user;
			let announcement = new Announcement(req.body);
			await announcement.save();

			if (req.body.push)
				await this.pushAnnouncement(announcement);

			res.json(announcement);
		}
		catch (err) {
			next(err);
		}
	}

	async del(req, res, next) {
		try {
			let id = {_id: req.params.id};
			await Announcement.remove(id);
			res.json(id);
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = AnnouncementsApi;
