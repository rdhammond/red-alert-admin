const Route = require('../Route');
const PushToken = require('../../models/PushToken');

class PushTokensApi extends Route {
	register(app) {
		app.post('/api/pushtokens', this.authApiKey, this.post.bind(this));
	}

	async post(req, res, next) {
		try {
			let pushToken = new PushToken(req.body);
			await PushToken.findOneAndUpdate(
				{expoToken: pushToken.expoToken},
				pushToken,
				{upsert: true, new: true}
			);
			res.json(pushToken);
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = PushTokensApi;
