const Route = require('../Route');
const Pusher = require('../../lib/Pusher');
const Event = require('../../models/Event');

class EventsApi extends Route {
	constructor() {
		super();
		this.pusher = new Pusher();
	}

	register(app) {
		app.route('/api/events')
			.get(this.authApiKey, this.all.bind(this))
			.post(this.authJson, this.post.bind(this));

		app.get('/api/events/:id', this.authApiKey, this.one.bind(this));
		app.delete('/api/events/:id', this.authJson, this.del.bind(this));
	}

	async all(req, res, next) {
		try {
			let events = await Event
				.find({})
				.sort({eventTime: -1});

			res.json({events});
		}
		catch (err) {
			next(err);
		}
	}

	async one(req, res, next) {
		try {
			let ev = await Event.findById(req.params.id);
			res.json(ev);
		}
		catch (err) {
			next(err);
		}
	}

	async pushEvent(ev) {
		let date = ev.eventTime.toLocaleDateString('en-US', {
			month: 'short',
			day: 'numeric'
		});
		let time = ev.eventTime.toLocaleTimeString('en-US', {
			hour: 'numeric',
			minute: '2-digit',
			hour12: true
		});

		return this.pusher.pushMessage({
			title: `New Event: ${ev.title}`,
			body: `${date}, ${time}`,
			data: {
				type: 'event'
			}
		});
	}

	async post(req, res, next) {
		try {
			req.body.updatedBy = req.user;
			req.body.createdBy = req.user;

			let ev = new Event(req.body);
			await ev.save();

			if (req.body.push)
				this.pushEvent(ev);

			res.json(ev);
		}
		catch (err) {
			next(err);
		}
	}

	async del(req, res, next) {
		try {
			let id = {_id: req.params.id};
			await Event.remove(id);
			res.json(id);
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = EventsApi;
