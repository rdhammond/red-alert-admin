const Route = require('../Route');
const Geocoder = require('../../lib/Geocoder');

class LocationApi extends Route {
	constructor() {
		super();
		this.geocoder = new Geocoder();
	}

	register(app) {
		app.route('/api/locations')
			.post(this.authJson, this.post.bind(this));
	}

	async post(req, res, next) {
		try {
			let loc = await this.geocoder.locate(req.body.address);
			res.json({loc});
		}
		catch (err) {
			console.log(err);
			next(err);
		}
	}
}

module.exports = LocationApi;
