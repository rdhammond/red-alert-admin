const _ = require('underscore');
const Route = require('../Route');
const ContactInfo = require('../../models/ContactInfo');

class ContactInfoApi extends Route {
	register(app) {
		app.route('/api/contactinfo')
			.get(this.authApiKey, this.get.bind(this))
			.put(this.put.bind(this));
	}

	async get(req, res, next) {
		try {
			let contactInfo = await ContactInfo.findOne({});
			res.json(contactInfo);
		}
		catch (err) {
			next(err);
		}
	}

	async put(req, res, next) {
		try {
			let contactInfo = await ContactInfo.findOne({});

			if (contactInfo)
				contactInfo = _.extend(contactInfo, req.body);
			else
				contactInfo = new ContactInfo(req.body);

			await contactInfo.save();
			res.json(contactInfo);
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = ContactInfoApi;
