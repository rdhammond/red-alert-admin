const Route = require('./Route');

class SignOutRoute extends Route {
	register(app) {
		app.route('/signout')
			.get(this.get.bind(this));
	}

	get(req, res) {
		req.logout();
		res.redirect('/signin');
	}
}

module.exports = SignOutRoute;
