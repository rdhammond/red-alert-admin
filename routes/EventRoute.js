const Route = require('./Route');
const Geocoder = require('../lib/Geocoder');
const Event = require('../models/Event');

class EventRoute extends Route {
	constructor(config) {
		super();
		this.geocoder = new Geocoder(config);
	}

	register(app) {
		app.route('/events')
			.all(this.pre.bind(this))
			.get(this.authHtml, this.get.bind(this));
	}

	pre(req, res, next) {
		res.locals.current = 'events';
		next();
	}

	async get(req, res, next) {
		try {
			let events = await Event
				.find({})
				.sort({eventTime: -1});

			res.locals.events = events;
			res.render('events');
		}
		catch (err) {
			next(err);
		}
	}
}

module.exports = EventRoute;
